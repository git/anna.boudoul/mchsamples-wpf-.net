﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModelViewWPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal Manager Manager
        {
            get
            {
                return (App.Current as App).Manager;
            }
        }

        public MainWindow()
        {
            this.InitializeComponent();
            DataContext = Manager;
        }

        int id = 4;

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            Manager.Add(new Nounours { Nom = "Nom", Naissance = DateTime.Now, NbPoils = 0, Id = ++id });
            Manager.SelectedIndex = Manager.LesNounours.Count() - 1;
        }

        private void Remove_Button_Click(object sender, RoutedEventArgs e)
        {
            Manager.Remove(Manager.SelectedNounours);
            Manager.SelectedIndex = 0;
        }
    }
}
