﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ex_DataTemplateSelector
{
    class Forme : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public virtual double X
        {
            get { return x; }
            set { x = value; OnPropertyChanged(); }
        }
        private double x;

        public virtual double Y
        {
            get { return y; }
            set { y = value; OnPropertyChanged(); }
        }
        private double y;

    }
}
