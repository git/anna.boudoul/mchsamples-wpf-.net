﻿using System.Globalization;
using System.Windows.Controls;

namespace ex_ValidationWithValidationRule
{
    /// <summary>
    /// Règle de validation d'un string qui fait moins de 5 caractères 
    /// </summary>
    public class StringLessThan5Characters : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value != null && value.ToString().Length > 5)
            {
                return new ValidationResult(false, "Please enter a string with 5 characters max");
            }
            return new ValidationResult(true,null);
        }
    }
}