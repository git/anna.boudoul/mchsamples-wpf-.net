﻿using ContactsWPF.ViewModels;
using giContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ContactsWPF.Commands
{
    public class PhoneCallCommand : ICommand
    {
        private PersonneVM mPersonneVM;

        public PhoneCallCommand(PersonneVM personneVM)
        {
            mPersonneVM = personneVM;
        }

        public bool CanExecute(object parameter)
        {
            if (mPersonneVM == null || mPersonneVM.SelectedTéléphone == null)
            {
                return false;
            }
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            MessageBox.Show(string.Format("Appel {0} {1} ({2}) en cours...",
                                            mPersonneVM.Prénom,
                                            mPersonneVM.Nom,
                                            mPersonneVM.SelectedTéléphone.Numéro));
        }
    }
}
