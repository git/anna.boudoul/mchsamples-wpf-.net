﻿using giContacts;
using MyMVVMToolkit;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsWPF.ViewModels
{
    class MainVM : BaseViewModel
    {
        public ObservableCollectionWrapper<Personne, PersonneVM> Personnes
        {
            get
            {
                return mPersonnes;
            }
        }
        private ObservableCollectionWrapper<Personne, PersonneVM> mPersonnes 
            = new ObservableCollectionWrapper<Personne, PersonneVM>(giContacts.Personne.Personnes);

        public PersonneVM SelectedPersonne
        {
            get
            {
                return mSelectedPersonne;
            }
            set
            {
                SetProperty(ref mSelectedPersonne, value, () => SelectedPersonne);
            }
        }
        private PersonneVM mSelectedPersonne;

        
    }
}
