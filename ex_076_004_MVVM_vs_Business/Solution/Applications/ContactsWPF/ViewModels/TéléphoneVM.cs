﻿using giContacts;
using MyMVVMToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsWPF.ViewModels
{
    public class TéléphoneVM : BaseViewModel<Téléphone>
    {
        public static IEnumerable<string> TéléphoneTypes
        {
            get
            {
                return Enum.GetNames(typeof(Téléphone.TéléphoneType));
            }
        }

        public string Type
        {
            get
            {
                return Model.Type.ToString();
            }
            set
            {
                Téléphone.TéléphoneType result;
                if(!Enum.TryParse(value, out result))
                {
                    return;
                }
                Model.Type = result;
                OnPropertyChanged(() => Type);
            }
        }

        public string Numéro
        {
            get
            {
                return Model.Numéro;
            }
            set
            {
                Model.Numéro = value;
                OnPropertyChanged(() => Numéro);
            }
        }
    }
}
