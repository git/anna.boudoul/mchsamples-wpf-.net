﻿using System.Collections.Generic;
using System.Windows;

namespace ex_ValidationWithAttributes
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Person Person { get; set; }


        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            Person = new Person();
        }
    }
}
