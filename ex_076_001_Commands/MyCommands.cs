﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ex_076_001_Commands
{
    static class MyCommands
    {
        static readonly RoutedUICommand mScaleOneCommand =
            new RoutedUICommand("Scale 1:1", "Scale1",
            typeof(MyCommands));

        public static RoutedUICommand ScaleOneCommand
        {
            get
            {
                return mScaleOneCommand;
            }
        }
    }
}
