﻿##### ex_082_004_Navigation_From_and_To_UserControls

## What's the purpose of this sample?
This sample has been prepared to allow a navigation between views in a WPF application, where each 
view is represented by a UserControl. Every UserControl contains one or more button(s) whose clicks
lead to other UserControl. It has been asked by students to be simple, without the use of MVVM or Commands.  
  
In brief, the content of the MainWindow can be either a UserControl1 or a UserControl2.
UserControl1 contains a button which leads to UserControl2 and UserControl2 contains a button
which leads to UserControl1.
## How this sample works?
The steps to follow are:
- prepare the usercontrols,
- add event to usercontrols and link them to the button click events,
- create a Navigator class responsible of managing navigation,
- use the Navigator in MainWindow.
#### Prepare the UserControls
Add a new UserControl to your WPF app. Add one or more button and name them:
```` XAML
<Button x:Name="button"/>
````
#### Add event to UserControls and link them to the button click events
In the code-behind of the UserControl, add one event for each button in your XAML. 
These events should be of type RoutedEventHandler (like the Click event of a button).
Then, the operator add (+=) and remove (-=) of these events are written.
The following code says that, if you subscribe to the ButtonClick event of your UserControl,
you're actually and directly subscribing to the Click event of the _button_ element of your UserControl. 
```` C#
public event RoutedEventHandler ButtonClick
{
    add
    {
        button.Click += value;
    }
    remove
    {
        button.Click -= value;
    }
}
````
#### Create a Navigator class responsible of managing navigation
Add a new class and name it Navigator. This class should implements INotifyPropertyChanged and have a 
SelectedUserControl property sending the PropertyChanged event when this property is modified.
```` C#
public class Navigator : INotifyPropertyChanged
{
    public Navigator()
    {
    }

    public UserControl SelectedUserControl
    {
        get => selectedUserControl;
        set
        {
            selectedUserControl = value;
            OnPropertyChanged();
        }
    }
    private UserControl selectedUserControl;

    public event PropertyChangedEventHandler PropertyChanged;

    void OnPropertyChanged([CallerMemberName] string propertyName = "")
        => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedUserControl)));
}
````
This class instantiate every UserControl you want to navigate to.
```` C#
Dictionary<string, UserControl> userControls = new Dictionary<string, UserControl>()
{
    ["UC1"] = new UserControl1(),
    ["UC2"] = new UserControl2()
};
````
Then, subscribe navigation methods to every custom events of your UserControl:
```` C#
private void InitUserControls()
{
    (userControls["UC1"] as UserControl1).ButtonClick += (sender, args) => SelectedUserControl = userControls["UC2"];
    (userControls["UC2"] as UserControl2).ButtonClick += (sender, args) => SelectedUserControl = userControls["UC1"];
            
    SelectedUserControl = userControls["UC1"];
}

public Navigator()
{
    InitUserControls();
}
````
In the previous block of code, the Click on the button of UserControl1 selects the UserControl2;
the Click on the button of UserControl2 selects the UserControl1.
The method subscribed to ButtonClick of UserControl1 has subscribed to the Click event of the button
in UserControl1.

#### Use the Navigator in MainWindow
Finally, an instance of Navigator is added to the MainWindow:
```` C#
public partial class MainWindow : Window
{
    public Navigator Navigator { get; set; } = new Navigator();

    public MainWindow()
    {
        InitializeComponent();
        DataContext = this;
    }
}
````
The XAML part of the MainWindow only contains a ContentControl whose Content property is bound
to the SelectedUserControl of the Navigator property.
```` XAML
<ContentControl Content="{Binding Navigator.SelectedUserControl}"/>
````
   \
   \
Now you can adapt this sample to your needs. Happy coding!
  \
  \
  \
_Copyright (C) Marc Chevaldonné, 30th of May 2019_
