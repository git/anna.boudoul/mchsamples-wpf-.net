﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ex_custom_shaped_button
{
    class String2ImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri(string.Format("pack://application:,,,/ex_custom_shaped_button;Component{0}", value as string), UriKind.Absolute));
//            return new ImageBrush(new BitmapImage(new Uri(value as string, UriKind.Relative)));
            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
