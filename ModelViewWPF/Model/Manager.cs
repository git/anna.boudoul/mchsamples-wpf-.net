﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Manager : INotifyPropertyChanged
    {
        public ObservableCollection<Nounours> LesNounours
        {
            get
            {
                return lesNounours;
            }
        }
        private ObservableCollection<Nounours> lesNounours = new ObservableCollection<Nounours>();

        IDataManager DataManager { get; set; }

        public Manager(IDataManager dataManager)
        {
            DataManager = dataManager;
            foreach(var n in dataManager.LesNounours)
            {
                lesNounours.Add(n);
                n.PropertyChanged += NounoursPropertyChanged;
            }
        }

        private void NounoursPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Nounours nounours = sender as Nounours;
            if (nounours == null) return;
            Update(nounours);
        }

        private int selectedIndex;

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                OnPropertyChanged(nameof(SelectedIndex));
                OnPropertyChanged(nameof(SelectedNounours));
            }
        }

        public Nounours SelectedNounours
        {
            get
            {
                return SelectedIndex >= 0 ? lesNounours[SelectedIndex] : null;
            }
        }

        public void Add(Nounours nounours)
        {
            if (lesNounours.Contains(nounours)) return;
            lesNounours.Add(nounours);
            nounours.PropertyChanged += NounoursPropertyChanged;
            DataManager.Add(nounours);
        }

        //public void Add(string nom, int nbPoils, DateTime dateDeNaissance)
        //{
        //    DataManager.Add(new Nounours { Nom = nom, NbPoils = nbPoils, Naissance = dateDeNaissance });
        //    //et l'Id ?
        //}

        public void Remove(Nounours nounours)
        {
            lesNounours.Remove(nounours);
            nounours.PropertyChanged -= NounoursPropertyChanged;
            DataManager.Remove(nounours);
        }

        public void Update(Nounours nounours)
        {
            DataManager.Update(nounours);
        }
    }
}
