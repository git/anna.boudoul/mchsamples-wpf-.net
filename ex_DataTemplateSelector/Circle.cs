﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex_DataTemplateSelector
{
    class Circle : Forme
    {
        public double Diameter
        {
            get { return diameter; }
            set { diameter = value; OnPropertyChanged(); }
        }
        private double diameter;

        public override double X { get => base.X - Diameter*0.5; set => base.X = value; }

        public override double Y { get => base.Y - Diameter * 0.5; set => base.Y = value; }

    }
}
