﻿using System.Windows;

namespace ex_ValidationWithValidationRule
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Person Person { get; set; }


        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            Person = new Person();
        }
    }
}
