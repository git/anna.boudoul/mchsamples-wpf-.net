﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace giContacts
{
    /// <summary>
    /// une adresse d'une personne
    /// </summary>
    public class Adresse
    {
        /// <summary>
        /// le numéro de la maison ou du bâtiment dans la rue
        /// </summary>
        public int Numéro
        {
            get;
            set;
        }

        /// <summary>
        /// le nom de la rue
        /// </summary>
        public string Rue
        {
            get;
            set;
        }

        /// <summary>
        /// la ville
        /// </summary>
        public string Ville
        {
            get;
            set;
        }

        /// <summary>
        /// le code postal
        /// </summary>
        public int CodePostal
        {
            get;
            set;
        }

        /// <summary>
        /// les différents types d'Adresse possibles
        /// </summary>
        public enum AdresseType
        {
            Inconnu,
            Domicile,
            Travail
        }

        /// <summary>
        /// le type de cette adresse
        /// </summary>
        public AdresseType Type
        {
            get;
            set;
        }
    }
}
