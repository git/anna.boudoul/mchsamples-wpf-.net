﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace giUserControls
{
    /// <summary>
    /// Logique d'interaction pour UserControlAdresse.xaml
    /// </summary>
    public partial class UserControlAdresse : UserControl
    {
        public UserControlAdresse()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty TypeProperty;
        public static readonly DependencyProperty NuméroProperty;
        public static readonly DependencyProperty VoieProperty;
        public static readonly DependencyProperty CodePostalProperty;
        public static readonly DependencyProperty VilleProperty;

        static UserControlAdresse()
        {
            UserControlAdresse.TypeProperty = 
                DependencyProperty.Register("Type",
                                            typeof(string),
                                            typeof(UserControlAdresse), 
                                            new PropertyMetadata("Travail"));

            UserControlAdresse.NuméroProperty =
                DependencyProperty.Register("Numéro",
                                            typeof(string),
                                            typeof(UserControlAdresse),
                                            new PropertyMetadata("99"));

            UserControlAdresse.VoieProperty =
                DependencyProperty.Register("Voie",
                                            typeof(string),
                                            typeof(UserControlAdresse),
                                            new PropertyMetadata("rue sympa"));

            UserControlAdresse.CodePostalProperty =
                DependencyProperty.Register("CodePostal",
                                            typeof(string),
                                            typeof(UserControlAdresse),
                                            new PropertyMetadata("76543"));

            UserControlAdresse.VilleProperty =
                DependencyProperty.Register("Ville",
                                            typeof(string),
                                            typeof(UserControlAdresse),
                                            new PropertyMetadata("Ville sympa"));
        }


        public string Numéro
        {
            get
            {
                return GetValue(UserControlAdresse.NuméroProperty) as string;
            }
            set
            {
                SetValue(UserControlAdresse.NuméroProperty, value);
            }
        }

        public string Voie
        {
            get
            {
                return GetValue(UserControlAdresse.VoieProperty) as string;
            }
            set
            {
                SetValue(UserControlAdresse.VoieProperty, value);
            }
        }

        public string CodePostal
        {
            get
            {
                return GetValue(UserControlAdresse.CodePostalProperty) as string;
            }
            set
            {
                SetValue(UserControlAdresse.CodePostalProperty, value);
            }
        }

        public string Ville
        {
            get
            {
                return GetValue(UserControlAdresse.VilleProperty) as string;
            }
            set
            {
                SetValue(UserControlAdresse.VilleProperty, value);
            }
        }

        public string Type
        {
            get
            {
                return GetValue(UserControlAdresse.TypeProperty) as string;
            }
            set
            {
                SetValue(UserControlAdresse.TypeProperty, value);
            }
        }
    }
}
