﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ex_DataTemplateSelector
{
    class FormeTemplateSelector : DataTemplateSelector
    {
        public DataTemplate CircleTemplate
        { get; set; }

        public DataTemplate RectangleTemplate
        {
            get; set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is Circle) return CircleTemplate;
            if (item is Rectangle) return RectangleTemplate;
            return base.SelectTemplate(item, container);
        }
    }
}
