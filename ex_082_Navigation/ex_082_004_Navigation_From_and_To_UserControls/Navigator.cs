﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ex_082_004_Navigation_From_and_To_UserControls
{
    public class Navigator : INotifyPropertyChanged
    {
        Dictionary<string, UserControl> userControls = new Dictionary<string, UserControl>()
        {
            ["UC1"] = new UserControl1(),
            ["UC2"] = new UserControl2()
        };

        private void InitUserControls()
        {
            (userControls["UC1"] as UserControl1).ButtonClick += (sender, args) => SelectedUserControl = userControls["UC2"];
            (userControls["UC2"] as UserControl2).ButtonClick += (sender, args) => SelectedUserControl = userControls["UC1"];
            
            SelectedUserControl = userControls["UC1"];
        }

        public Navigator()
        {
            InitUserControls();
        }

        public UserControl SelectedUserControl
        {
            get => selectedUserControl;
            set
            {
                selectedUserControl = value;
                OnPropertyChanged();
            }
        }
        private UserControl selectedUserControl;

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string propertyName = "")
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedUserControl)));
    }
}
