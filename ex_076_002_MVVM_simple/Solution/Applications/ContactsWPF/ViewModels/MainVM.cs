﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsWPF.ViewModels
{
    class MainVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        public ObservableCollection<PersonneVM> Personnes
        {
            get
            {
                return mPersonnes;
            }
        }
        private ObservableCollection<PersonneVM> mPersonnes = new ObservableCollection<PersonneVM>(giContacts.Personne.Personnes.Select(pers => new PersonneVM { Model = pers }));

        public PersonneVM SelectedPersonne
        {
            get
            {
                return mSelectedPersonne;
            }
            set
            {
                mSelectedPersonne = value;
                OnPropertyChanged("SelectedPersonne");
            }
        }
        private PersonneVM mSelectedPersonne;

        
    }
}
