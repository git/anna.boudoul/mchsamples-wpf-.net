﻿using ContactsWPF.ViewModels;
using giContacts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsWPF.Commands
{
    public class AddPhoneCommand : ICommand
    {
        private PersonneVM mPersonneVM;

        public AddPhoneCommand(PersonneVM personneVM)
        {
            mPersonneVM = personneVM;
        }

        public bool CanExecute(object parameter)
        {
            if (mPersonneVM == null)
            {
                return false;
            }
            if (!(parameter is string))
            {
                return false;
            }
            switch (parameter as string)
            {
                case "Add":
                    break;
                case "Modify":
                    if (mPersonneVM.SelectedTéléphone == null)
                    {
                        return false;
                    }
                    break;
                default:
                    return false;
            }
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            if(!(parameter is string))
            {
                return ;
            }
            string title;
            Views.WindowNewPhone windowNewPhone = new Views.WindowNewPhone();
            switch (parameter as string)
            {
                default:
                case "Add":
                    title = "Ajouter un nouveau numéro de téléphone";
                    break;
                case "Modify":
                    title = "Modifier un numéro de téléphone";
                    windowNewPhone.ViewModel.Téléphone = mPersonneVM.SelectedTéléphone;
                    break;
            }
            windowNewPhone.ViewModel.Title = title;
            windowNewPhone.Closing += windowNewPhone_Closing;
            windowNewPhone.ShowDialog();
        }

        void windowNewPhone_Closing(object sender, CancelEventArgs e)
        {
            var window = sender as Views.WindowNewPhone;

            mPersonneVM.AddPhone(window.ViewModel.Téléphone);

        }
    }
}
