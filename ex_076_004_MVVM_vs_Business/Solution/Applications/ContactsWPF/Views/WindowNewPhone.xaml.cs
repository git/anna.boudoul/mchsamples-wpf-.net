﻿using ContactsWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ContactsWPF.Views
{
    /// <summary>
    /// Logique d'interaction pour WindowNewPhone.xaml
    /// </summary>
    public partial class WindowNewPhone : Window
    {
        public WindowNewPhone(PersonneVM personne, WindowNewPhoneVM.AddOrModify type)
        {
            ViewModel = new ViewModels.WindowNewPhoneVM(personne, type, this);
            InitializeComponent();
            DataContext = ViewModel;
        }

        public ViewModels.WindowNewPhoneVM ViewModel
        {
            get;
            set;
        }
    }
}
