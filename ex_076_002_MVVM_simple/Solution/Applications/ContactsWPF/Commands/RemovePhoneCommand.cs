﻿using ContactsWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsWPF.Commands
{
    public class RemovePhoneCommand : ICommand
    {
        private PersonneVM mPersonneVM;

        public RemovePhoneCommand(PersonneVM personneVM)
        {
            mPersonneVM = personneVM;
        }

        public bool CanExecute(object parameter)
        {
            if (mPersonneVM == null || mPersonneVM.SelectedTéléphone == null)
            {
                return false;
            }
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            mPersonneVM.RemovePhone(mPersonneVM.SelectedTéléphone);
        }
    }
}
