﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ex_082_003_WindowNavigation_Commands
{
    public class NavigationVM
    {
        public ICommand NavigateCommand { get; set; }

        public NavigationVM()
        {
            NavigateCommand = new NavigateCommand();
        }
    }

    
}
