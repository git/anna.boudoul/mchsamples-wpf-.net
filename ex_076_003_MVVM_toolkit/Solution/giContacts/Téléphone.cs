﻿using MyMVVMToolkit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace giContacts
{
    public enum TéléphoneType
    {
        Inconnu,
        Mobile,
        Domicile,
        Travail,
        Fax
    }
    
    /// <summary>
    /// un numéro de téléphone d'une personne
    /// </summary>
    public class Téléphone: ObservableObject
    {
        /// <summary>
        /// le type de ce numéro de téléphone
        /// </summary>
        public TéléphoneType Type
        {
            get
            {
                return mType;
            }
            set
            {
                SetProperty(ref mType, value, () => Type);
            }
        }
        private TéléphoneType mType;

        /// <summary>
        /// le numéro de téléphone
        /// </summary>
        public string Numéro
        {
            get
            {
                return mNuméro;
            }
            set
            {
                SetProperty(ref mNuméro, value, () => Numéro);
            }
        }
        private string mNuméro;
    }
}
