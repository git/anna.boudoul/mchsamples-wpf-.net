﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using giContacts;
using System.Windows.Input;
using MyMVVMToolkit;
using System.Windows;
using System.ComponentModel;

namespace ContactsWPF.ViewModels
{
    public class PersonneVM : BaseViewModel<Personne>
    {
        public override Personne Model
        {
            get
            {
                return base.Model;
            }
            set
            {
                base.Model = value;
                mTéléphones = new ObservableCollectionWrapper<Téléphone, TéléphoneVM>(Model.Téléphones);
                mAdresses = new ObservableCollectionWrapper<Adresse, AdresseVM>(Model.Adresses);
            }
        }

        public string Nom
        {
            get
            {
                return Model.Nom;
            }
            set
            {
                Model.Nom = value;
                OnPropertyChanged(() => Nom);
                OnPropertyChanged(() => Self);
            }
        }

        public string Prénom
        {
            get
            {
                return Model.Prénom;
            }
            set
            {
                Model.Prénom = value;
                OnPropertyChanged(() => Prénom);
                OnPropertyChanged(() => Self);
            }
        }

        public string ImageSource
        {
            get
            {
                return Model.ImageSource;
            }
            set
            {
                Model.ImageSource = value;
                OnPropertyChanged(() => ImageSource);
                OnPropertyChanged(() => Self);
            }
        }

        public PersonneVM Self
        {
            get
            {
                return this;
            }
        }

        public TéléphoneVM SelectedTéléphone
        {
            get
            {
                return mSelectedTéléphone;
            }
            set
            {
                mSelectedTéléphone = value;
            }
        }
        private TéléphoneVM mSelectedTéléphone;

        public ObservableCollectionWrapper<Téléphone, TéléphoneVM> Téléphones
        {
            get
            {
                return mTéléphones;
            }
        }
        ObservableCollectionWrapper<Téléphone, TéléphoneVM> mTéléphones;

        public ObservableCollectionWrapper<Adresse, AdresseVM> Adresses
        {
            get
            {
                return mAdresses;
            }
        }
        ObservableCollectionWrapper<Adresse, AdresseVM> mAdresses;

        public int NbTéléphones
        {
            get
            {
                return Model.NbTéléphones;
            }
        }

        public ICommand PhoneCalledCommand
        {
            get
            {
                return mPhoneCalledCommand ?? (mPhoneCalledCommand = new RelayCommand(
                    () =>
                    {
                        MessageBox.Show(string.Format("Appel {0} {1} ({2}) en cours...",
                                            this.Prénom,
                                            this.Nom,
                                            this.SelectedTéléphone.Numéro));
                    },
                    () =>
                    {
                        if (this.SelectedTéléphone == null)
                        {
                            return false;
                        }
                        return true;
                    }));
            }
        }
        private ICommand mPhoneCalledCommand;

        public ICommand AddPhoneCommand
        {
            get
            {
                return mAddPhoneCommand ?? (mAddPhoneCommand = new RelayCommand<WindowNewPhoneVM.AddOrModify>(
                    param =>
                    {
                        new Views.WindowNewPhone(this, param).ShowDialog(); 
                    },
                    param =>
                    {
                        switch (param)
                        {
                            case WindowNewPhoneVM.AddOrModify.Add:
                                break;
                            case WindowNewPhoneVM.AddOrModify.Modify:
                                if (this.SelectedTéléphone == null)
                                {
                                    return false;
                                }
                                break;
                            default:
                                return false;
                        }
                        return true;
                    }));
            }
        }
        private ICommand mAddPhoneCommand;

        public ICommand RemovePhoneCommand
        {
            get
            {
                return mRemovePhoneCommand ?? (mRemovePhoneCommand = new RelayCommand(
                    () =>
                    {
                        this.RemovePhone(this.SelectedTéléphone);
                    },
                    () =>
                    {
                        if (this == null || this.SelectedTéléphone == null)
                        {
                            return false;
                        }
                        return true;
                    }));
            }
        }
        private ICommand mRemovePhoneCommand;

        public void RemovePhone(TéléphoneVM phone)
        {
            Téléphones.Remove(phone);
            OnPropertyChanged(() => NbTéléphones);
        }

        public void AddPhone(TéléphoneVM phone)
        {
            Téléphones.Add(phone);
            OnPropertyChanged(() => NbTéléphones);

        }
    }
}
