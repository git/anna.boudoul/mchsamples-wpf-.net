﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ex_custom_shaped_button
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public static List<string> imgs = new List<string>
                                        {
                                            @"/images/horace01.jpg",
                                            @"/images/horace02.jpg",
                                            @"/images/horace03.jpg",
                                            @"/images/horace04.jpg"
                                        };

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show($"Click on {(sender as Button).Content}");
        }
    }
}
