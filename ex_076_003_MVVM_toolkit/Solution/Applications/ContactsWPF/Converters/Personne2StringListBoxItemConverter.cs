﻿using ContactsWPF.ViewModels;
using giContacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContactsWPF.Converters
{
    [System.Windows.Data.ValueConversion(typeof(PersonneVM), typeof(String))]
    public class Personne2StringListBoxItemConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var personne = value as PersonneVM;
            if (personne == null)
            {
                return null;
            }

            return string.Format("{0} {1}", personne.Prénom, personne.Nom.ToUpper());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
