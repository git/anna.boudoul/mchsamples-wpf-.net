﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ex_SimpleVisualStates
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Méthode qui change l'état de notre élément 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGoToSmall(object sender, RoutedEventArgs e)
        {
            //Changement d'état, l'état "Small" est appliqué sur la vue
            VisualStateManager.GoToElementState(MyGrid, "Small", true);
        }

        /// <summary>
        ///  Méthode qui change l'état de notre élément 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGoToLarge(object sender, RoutedEventArgs e)
        {
            //Changement d'état, l'état "Large" est appliqué sur la vue
            VisualStateManager.GoToElementState(MyGrid, "Large", true);
        }
    }
}
